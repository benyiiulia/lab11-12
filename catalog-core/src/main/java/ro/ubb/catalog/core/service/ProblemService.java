package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.model.Problem;
import ro.ubb.catalog.core.repo.ProblemRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ProblemService  {
    public static final Logger log = LoggerFactory.getLogger(ProblemService.class);
    @Autowired
    ProblemRepository repository;

    public List<Problem> findAll() {
        return repository.findAll();
    }

    public Problem save(Problem entity) {
        return repository.save(entity);
    }

    @Transactional
    public Problem update(Problem entity) {
        repository.findById(entity.getId()).ifPresent(e -> { e.update(entity); });
        return entity;
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    public Set<Problem> filterProblemsByText(String text) {
        log.trace("filterProblemsByName: name={}", text);
        Iterable<Problem> problems = this.repository.findAll();
        Set<Problem> filteredProblems = new HashSet<>();
        problems.forEach(filteredProblems::add);
        filteredProblems.removeIf(problem -> !problem.getText().contains(text));
        return filteredProblems;
    }
}
