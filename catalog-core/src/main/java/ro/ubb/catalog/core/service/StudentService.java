package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.core.repo.StudentRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class StudentService  {
    public static final Logger log = LoggerFactory.getLogger(StudentService.class);
    @Autowired
    StudentRepository repository;

    public List<Student> findAll() {
        return repository.findAll();
    }

    public Student save(Student entity) {
        return repository.save(entity);
    }

    @Transactional
    public Student update(Student entity) {
        repository.findById(entity.getId()).ifPresent(e -> { e.update(entity); });
        return entity;
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    public Set<Student> filterStudentsByName(String name) {
        log.trace("filterStudentsByName: name={}", name);
        Iterable<Student> students = this.repository.findAll();
        Set<Student> filteredStudents = new HashSet<>();
        students.forEach(filteredStudents::add);
        filteredStudents.removeIf(student -> !student.getName().contains(name));
        return filteredStudents;
    }
}
