package ro.ubb.catalog.core.repo;

import org.springframework.stereotype.Repository;
import ro.ubb.catalog.core.model.Problem;

@Repository
public interface ProblemRepository extends CatalogRepository<Problem, Integer> {
}
