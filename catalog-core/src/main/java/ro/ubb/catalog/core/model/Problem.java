package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
public class Problem extends BaseEntity<Integer> {
    private String text;

    public Problem(int id, String text) {
        this.id = id;
        this.text = text;
    }

    @Override
    public void update(BaseEntity<Integer> other) {
        if(!(other instanceof Problem)) {
            return;
        }
        var other_ = (Problem) other;
        this.text = other_.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String toString()
    {
        return "ID = " + id + "\n" +
                "Text = " + text + "\n";
    }
}
