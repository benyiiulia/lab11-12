package ro.ubb.catalog.core.service;

import ro.ubb.catalog.core.model.Grade;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.model.Rental;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.catalog.core.repo.GradeRepository;
import ro.ubb.catalog.core.repo.RentalRepository;

import javax.transaction.Transactional;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class GradeService {
    public static final Logger log = LoggerFactory.getLogger(GradeService.class);
    @Autowired
    private StudentService studentService;
    @Autowired
    private ProblemService problemService;
    @Autowired
    GradeRepository repository;

    public List<Grade> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Grade save(Grade entity) {
//        Movie movie = new Movie(movieService.findById(entity.getMovieId()));
//        movie.setIsRented(true);
//        movieService.update(movie);
        return repository.save(entity);
    }

    @Transactional
    public Grade update(Grade entity) {
        repository.findById(entity.getId()).ifPresent(e -> { e.update(entity); });
        return entity;
    }

    public Grade findById(Integer id) {
        var byId = repository.findById(id);
        return byId.orElseGet(Grade::new);
    }

    @Transactional
    public void deleteById(Integer id) {
        Grade entity = findById(id);
//        Movie movie = new Movie(movieService.findById(entity.getMovieId()));
//        movie.setIsRented(false);
//        movieService.update(movie);
        repository.deleteById(id);
    }

//    public Set<Rental> filterRentalsByClient(int clientId) {
//        log.trace("filterRentalsByClient: clientId={}", clientId);
//        Iterable<Rental> rentals = repository.findAll();
//        Set<Rental> filteredRentals = new HashSet<>();
//        rentals.forEach(filteredRentals::add);
//        filteredRentals.removeIf(rental -> rental.getClientId() != clientId);
//        return filteredRentals;
//    }
//
//    public Set<Rental> filterRentalsByMovie(int movieId) {
//        log.trace("filterRentalsByMovie: movieId={}", movieId);
//        Iterable<Rental> rentals = repository.findAll();
//        Set<Rental> filteredRentals = new HashSet<>();
//        rentals.forEach(filteredRentals::add);
//        filteredRentals.removeIf(rental -> rental.getMovieId() != movieId);
//        return filteredRentals;
//    }
//
//    public Movie getMostRentedMovie() throws Exception {
//        log.trace("getMostRentedMovie: --");
//        int movieId = (repository.findAll()).stream()
//                .map(Rental::getMovieId)
//                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
//                .entrySet()
//                .stream()
//                .max(Map.Entry.comparingByValue())
//                .map(Map.Entry::getKey)
//                .orElseThrow(() -> new Exception("No rentals in repo"));
//        return movieService.getOne(movieId);
//    }
}
