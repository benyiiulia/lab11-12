package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
public class Student extends BaseEntity<Integer> {
    private String name;
    private String serialNumber;
    private int groupNumber;

    public Student(int id, String name, String serialNumber, int groupNumber) {
        this.id = id;
        this.name = name;
        this.serialNumber = serialNumber;
        this.groupNumber = groupNumber;
    }

    @Override
    public void update(BaseEntity<Integer> other) {
        if(!(other instanceof Client)) {
            return;
        }
        var other_ = (Student) other;
        this.name = other_.name;
        this.serialNumber = other_.serialNumber;
        this.groupNumber = other_.groupNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSerialNumber(String serialNumber){ this.serialNumber = serialNumber;}

    public void setGroupNumber(int groupNumber){this.groupNumber = groupNumber;}


    public String getName() {
        return name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public String toString()
    {
        return "ID = " + id + "\n" +
                "SerialNumber = " + serialNumber + "\n" +
                "Name = " + name + "\n" +
                "Group = " + groupNumber;
    }
}
