package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
public class Grade extends BaseEntity<Integer> {
    private int studentID;
    private int problemID;
    private int grade;

    public Grade(int id, int studentID, int problemID, int grade) {
        this.id = id;
        this.studentID = studentID;
        this.problemID = problemID;
        this.grade = grade;
    }

    @Override
    public void update(BaseEntity<Integer> other) {
        if(!(other instanceof Grade)) {
            return;
        }
        var other_ = (Grade) other;
        this.studentID = other_.studentID;
        this.problemID = other_.problemID;
        this.grade = other_.grade;
    }

    public int getStudentID() {
        return studentID;
    }

    public int getProblemID() {
        return problemID;
    }

    public int getGrade() {
        return grade;
    }

    public void setStudentID(int value) {
        studentID = value;
    }

    public void setProblemID(int value) {
        problemID = value;
    }

    public void setGrade(int value) {
        grade = value;
    }

    public String toString()
    {
        return "id = " + id + "\n" +
                "studentID = " + studentID + "\n" +
                "problemID = " + problemID + "\n" +
                "grade = " + grade;
    }
}
