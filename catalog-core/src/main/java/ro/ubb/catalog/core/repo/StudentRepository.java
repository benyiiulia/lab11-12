package ro.ubb.catalog.core.repo;

import org.springframework.stereotype.Repository;
import ro.ubb.catalog.core.model.Student;

@Repository
public interface StudentRepository extends CatalogRepository<Student, Integer> {
}
