package ro.ubb.catalog.core.repo;

import ro.ubb.catalog.core.model.Grade;
import org.springframework.stereotype.Repository;

@Repository
public interface GradeRepository extends CatalogRepository<Grade, Integer>{
}
