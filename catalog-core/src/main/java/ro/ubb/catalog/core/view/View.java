//package ro.ubb.catalog.core.view;
//
//import ro.ubb.catalog.core.model.Client;
//import ro.ubb.catalog.core.model.Movie;
//import ro.ubb.catalog.core.model.Rental;
//import org.hibernate.service.spi.ServiceException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import ro.ubb.catalog.core.service.ClientService;
//import ro.ubb.catalog.core.service.MovieService;
//import ro.ubb.catalog.core.service.RentalService;
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.util.InputMismatchException;
//import java.util.Objects;
//import java.util.Optional;
//import java.util.Set;
//
//import static java.lang.System.exit;
//
//
//@Component
//public class View {
//    public static final Logger log = LoggerFactory.getLogger(View.class);
//    @Autowired
//    private MovieService movieService;
//    @Autowired
//    private ClientService clientService;
//    @Autowired
//    private RentalService rentalService;
//
//    private Movie readMovie() {
//        System.out.println("Read movie {id, name, director, trailer, rating, category}");
//
//        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            int id = Integer.parseInt(bufferRead.readLine());// ...
//            String movieName = bufferRead.readLine();
//            String movieDirector = bufferRead.readLine();
//            String movieTrailer = bufferRead.readLine();// ...
//            int movieRating = Integer.parseInt(bufferRead.readLine());
//            String movieCategory = bufferRead.readLine();
//
//            return new Movie(id, movieName, movieDirector, movieTrailer, movieRating, movieCategory);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    private Client readClient() {
//        System.out.println("Read client {id, name, age, address}");
//        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            int id = Integer.parseInt(bufferRead.readLine());// ...
//            String name = bufferRead.readLine();
//            int age = Integer.parseInt(bufferRead.readLine());
//            String address = bufferRead.readLine();
//
//            return new Client(id, name, age, address);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    private Rental readRental() {
//        System.out.println("Read rental {id, clientId, movieId}");
//        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            int id = Integer.parseInt(bufferRead.readLine());
//            int clientId = Integer.parseInt(bufferRead.readLine());
//            int movieId = Integer.parseInt(bufferRead.readLine());
//            return new Rental(id, clientId, movieId);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//
//    private int readId() {
//        System.out.println("Enter {id}");
//        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            return Integer.parseInt(bufferRead.readLine());// ...
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        return -1;
//    }
//
//    private void addMovieDialog() {
//        this.movieService.save(readMovie());
//    }
//
//    private void removeMovieDialog()  {
//        this.movieService.deleteById(readId());
//    }
//
//    private void updateMovieDialog() {
//        this.movieService.update(Objects.requireNonNull(readMovie()));
//    }
//
//    private void filterMoviesDialog() {
//        System.out.println("filter movies by name");
//        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            String name = bufferRead.readLine();
//            Set<Movie> movies = this.movieService.filterMoviesByName(name);
//            movies.forEach(System.out::println);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    private void addClientDialog() {
//        this.clientService.save(readClient());
//    }
//
//    private void removeClientDialog(){
//        this.clientService.deleteById(readId());
//    }
//
//    private void updateClientDialog() {
//        this.clientService.update(Objects.requireNonNull(readClient()));
//    }
//
//    private void filterClientsDialog() {
//        System.out.println("filter clients by name");
//        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            String name = bufferRead.readLine();
//            Set<Client> clients = this.clientService.filterClientsByName(name);
//            clients.forEach(System.out::println);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    private void addRental(){
//        Rental rental = readRental();
//        rentalService.save(rental);
//    }
//
//    private void removeRental(){
//        int id = readId();
//        rentalService.deleteById(id);
//    }
//
//    private void filterRentalsByClient()  {
//        System.out.println("filter rentals by client id");
//        int clientId = readId();
//        Set<Rental> rentals = this.rentalService.filterRentalsByClient(clientId);
//        rentals.forEach(System.out::println);
//    }
//
//    private void filterRentalsByMovie(){
//        System.out.println("filter rentals by movie id");
//        int movieId = readId();
//        Set<Rental> rentals = this.rentalService.filterRentalsByMovie(movieId);
//        rentals.forEach(System.out::println);
//    }
//
//    private void getMostRentedMovie() throws Exception {
//        System.out.println(rentalService.getMostRentedMovie().toString());
//    }
//
//    private void printMenu() {
//        System.out.println("Menu:");
//        System.out.println("1)Add Movie.");
//        System.out.println("2)Remove Movie.");
//        System.out.println("3)Update Movie.");
//        System.out.println("4)Filter movies.");
//        System.out.println("5)Print all movies.");
//        System.out.println("6)Add Client.");
//        System.out.println("7)Remove Client.");
//        System.out.println("8)Update Movie.");
//        System.out.println("9)Filter clients.");
//        System.out.println("10)Print all clients.");
//        System.out.println("11)Add rental");
//        System.out.println("12)Remove rental");
//        System.out.println("13)Filter rentals by client");
//        System.out.println("14)Filter rentals by movies");
//        System.out.println("15)Get most rented movie");
//        System.out.println("16)Print all rentals");
//        System.out.println("0)Exit");
//        System.out.println("Enter command:");
//    }
//
//    private int getCommand() {
//        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            int command = Integer.parseInt(bufferRead.readLine());
//            System.out.print("\n");
//            return command;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return -1;
//        }
//    }
//
//    public void run(){
//        while(true)
//        {
//            try {
//                this.printMenu();
//                int command = this.getCommand();
//                log.trace("run: command={}", command);
//                Optional.of(command).filter(e->e==1).ifPresent(go->{this.addMovieDialog();});
//                Optional.of(command).filter(e->e==2).ifPresent(go->{
//                        this.removeMovieDialog();
//                });
//                Optional.of(command).filter(e->e==3).ifPresent(go->{this.updateMovieDialog();});
//                Optional.of(command).filter(e->e==4).ifPresent(go->{this.filterMoviesDialog();});
//                Optional.of(command).filter(e->e==5).ifPresent(go->{
//                        System.out.println(this.movieService.findAll());
//                });
//                Optional.of(command).filter(e->e==6).ifPresent(go->{this.addClientDialog();});
//                Optional.of(command).filter(e->e==7).ifPresent(go->{
//                        this.removeClientDialog();
//                });
//                Optional.of(command).filter(e->e==8).ifPresent(go->{this.updateClientDialog();});
//                Optional.of(command).filter(e->e==9).ifPresent(go->{this.filterClientsDialog();});
//                Optional.of(command).filter(e->e==10).ifPresent(go->{
//                        System.out.println(this.clientService.findAll());
//                });
//                Optional.of(command).filter(e->e==11).ifPresent(go->{
//                        this.addRental();
//                });
//                Optional.of(command).filter(e->e==12).ifPresent(go->{
//                        this.removeRental();
//                });
//                Optional.of(command).filter(e->e==13).ifPresent(go->{
//                    this.filterRentalsByClient();
//                });
//                Optional.of(command).filter(e->e==14).ifPresent(go->{
//                        this.filterRentalsByMovie();
//                });
//                Optional.of(command).filter(e->e==15).ifPresent(go->{
//                    try {
//                        this.getMostRentedMovie();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                });
//                Optional.of(command).filter(e->e==16).ifPresent(go->{
//                        System.out.println(this.rentalService.findAll());
//                });
//                Optional.of(command).filter(e->e==0).ifPresent(go->{
//                    exit(0);
//                });
//            }
//            catch(InputMismatchException | NumberFormatException e){
//                System.out.println("Invalid input!\n");
//            }
//            catch(ServiceException e){
//                System.out.print(e.getMessage());
//            }
//        }
//    }
//}
