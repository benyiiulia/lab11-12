package ro.ubb.catalog.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;
import ro.ubb.catalog.client.view.View;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.ClientsDto;

public class ClientMain {
    public static final String URL = "http://localhost:8080/api/clients";
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro.ubb.catalog.client.config");
        System.out.println("ok here");
        RestTemplate restTemplate = context.getBean(RestTemplate.class);
        View view = new View(restTemplate);
        view.run();
    }
}
