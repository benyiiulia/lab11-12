package ro.ubb.catalog.client.view;

import org.springframework.web.client.RestTemplate;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.model.Rental;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.core.service.MovieService;
import ro.ubb.catalog.core.service.RentalService;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.converter.MovieConverter;
import ro.ubb.catalog.web.converter.RentalConverter;
import ro.ubb.catalog.web.dto.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.lang.System.exit;


@Component
public class View {
    private static final String CLIENT_URL = "http://localhost:8080/api/clients";
    private static final String MOVIE_URL = "http://localhost:8080/api/movies";
    private static final String RENTAL_URL = "http://localhost:8080/api/rentals";

    private ClientConverter clientConverter;
    private MovieConverter movieConverter;
    private RentalConverter rentalConverter;

    private RestTemplate restTemplate;


    public View(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        clientConverter = new ClientConverter();
        movieConverter = new MovieConverter();
        rentalConverter = new RentalConverter();
    }

    private String addIdToUrl(String url, Integer id) {
        return url.concat("/").concat(Integer.toString(id));
    }

    private String addStringToUrl(String url, String other) {
        return url.concat("/").concat(other);
    }

    private Movie readMovie() {
        System.out.println("Read movie {id, name, director, trailer, rating, category}");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            int id = Integer.parseInt(bufferRead.readLine());// ...
            String movieName = bufferRead.readLine();
            String movieDirector = bufferRead.readLine();
            String movieTrailer = bufferRead.readLine();// ...
            int movieRating = Integer.parseInt(bufferRead.readLine());
            String movieCategory = bufferRead.readLine();

            return new Movie(id, movieName, movieDirector, movieTrailer, movieRating, movieCategory);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Client readClient() {
        System.out.println("Read client {id, name, age, address}");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            int id = Integer.parseInt(bufferRead.readLine());// ...
            String name = bufferRead.readLine();
            int age = Integer.parseInt(bufferRead.readLine());
            String address = bufferRead.readLine();

            return new Client(id, name, age, address);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Rental readRental() {
        System.out.println("Read rental {id, clientId, movieId}");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            int id = Integer.parseInt(bufferRead.readLine());
            int clientId = Integer.parseInt(bufferRead.readLine());
            int movieId = Integer.parseInt(bufferRead.readLine());
            return new Rental(id, clientId, movieId);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private int readId() {
        System.out.println("Enter {id}");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Integer.parseInt(bufferRead.readLine());// ...
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    private void addMovieDialog() {
        MovieDto movieDto = movieConverter.convertModelToDto(readMovie());
        restTemplate.postForObject(
                MOVIE_URL,
                movieDto,
                MovieDto.class
        );
    }

    private void removeMovieDialog()  {
        var id = readId();
        restTemplate.delete(addIdToUrl(MOVIE_URL, id));
    }

    private void updateMovieDialog() {
        var movieDto = movieConverter.convertModelToDto(readMovie());
        var id = readId();
        restTemplate.put(addIdToUrl(MOVIE_URL, id), movieDto, MovieDto.class);
    }

    private void filterMoviesDialog() {
        System.out.println("filter movies by name");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            String name = bufferRead.readLine();
            String requestString = MOVIE_URL;
            requestString = addStringToUrl(requestString, "filterByName");
            requestString = addStringToUrl(requestString, name);
            MoviesDto moviesDto = restTemplate.getForObject(requestString, MoviesDto.class);
            Set<Movie> movies = movieConverter.convertDtosToModels(moviesDto.getMovies());
            movies.forEach(System.out::println);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addClientDialog() {
        ClientDto clientDto = clientConverter.convertModelToDto(readClient());
        restTemplate.postForObject(
                CLIENT_URL,
                clientDto,
                ClientDto.class
        );
    }

    private void removeClientDialog() {
        var id = readId();
        restTemplate.delete(addIdToUrl(CLIENT_URL, id));
    }

    private void updateClientDialog() {
        var clientDto = clientConverter.convertModelToDto(readClient());
        var id = readId();
        restTemplate.put(addIdToUrl(CLIENT_URL, id), clientDto, ClientDto.class);
    }

    private void filterClientsDialog() {
        System.out.println("filter clients by name");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            String name = bufferRead.readLine();
            String requestString = CLIENT_URL;
            requestString = addStringToUrl(requestString, "filterByName");
            requestString = addStringToUrl(requestString, name);
            ClientsDto clientsDto = restTemplate.getForObject(requestString, ClientsDto.class);
            Set<Client> clients = clientConverter.convertDtosToModels(clientsDto.getClients());
            clients.forEach(System.out::println);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void addRental() {
        RentalDto rentalDto = rentalConverter.convertModelToDto(readRental());
        restTemplate.postForObject(
                RENTAL_URL,
                rentalDto,
                RentalDto.class
        );
    }

    private void removeRental() {
        var id = readId();
        restTemplate.delete(addIdToUrl(RENTAL_URL, id));
    }

    private void filterRentalsByClient()  {
        System.out.println("filter rentals by client id");
        int clientId = readId();
        String requestString = RENTAL_URL;
        requestString = addStringToUrl(requestString, "filterByClient");
        requestString = addIdToUrl(requestString, clientId);
        RentalsDto rentalsDto = restTemplate.getForObject(requestString, RentalsDto.class);
        Set<Rental> rentals = rentalConverter.convertDtosToModels(rentalsDto.getRentals());
        rentals.forEach(System.out::println);
    }

    private void filterRentalsByMovie(){
        System.out.println("filter rentals by movie id");
        int movieId = readId();
        String requestString = RENTAL_URL;
        requestString = addStringToUrl(requestString, "filterByMovie");
        requestString = addIdToUrl(requestString, movieId);
        RentalsDto rentalsDto = restTemplate.getForObject(requestString, RentalsDto.class);
        Set<Rental> rentals = rentalConverter.convertDtosToModels(rentalsDto.getRentals());
        rentals.forEach(System.out::println);
    }

    private void getMostRentedMovie() throws Exception {
        String requestString = RENTAL_URL;
        requestString = addStringToUrl(requestString, "getMostRentedMovie");
        MovieDto movieDto = restTemplate.getForObject(requestString, MovieDto.class);
        System.out.println(movieConverter.convertDtoToModel(movieDto).toString());
    }

    private void printMenu() {
        System.out.println("Menu:");
        System.out.println("1)Add Movie.");
        System.out.println("2)Remove Movie.");
        System.out.println("3)Update Movie.");
        System.out.println("4)Filter movies.");
        System.out.println("5)Print all movies.");
        System.out.println("6)Add Client.");
        System.out.println("7)Remove Client.");
        System.out.println("8)Update Movie.");
        System.out.println("9)Filter clients.");
        System.out.println("10)Print all clients.");
        System.out.println("11)Add rental");
        System.out.println("12)Remove rental");
        System.out.println("13)Filter rentals by client");
        System.out.println("14)Filter rentals by movies");
        System.out.println("15)Get most rented movie");
        System.out.println("16)Print all rentals");
        System.out.println("0)Exit");
        System.out.println("Enter command:");
    }

    private int getCommand() {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            int command = Integer.parseInt(bufferRead.readLine());
            System.out.print("\n");
            return command;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public void run(){
        while(true)
        {
            try {
                this.printMenu();
                int command = this.getCommand();
                Optional.of(command).filter(e->e==1).ifPresent(go->{this.addMovieDialog();});
                Optional.of(command).filter(e->e==2).ifPresent(go->{
                    this.removeMovieDialog();
                });
                Optional.of(command).filter(e->e==3).ifPresent(go->{this.updateMovieDialog();});
                Optional.of(command).filter(e->e==4).ifPresent(go->{this.filterMoviesDialog();});
                Optional.of(command).filter(e->e==5).ifPresent(go->{
                    MoviesDto moviesDto = restTemplate.getForObject(MOVIE_URL, MoviesDto.class);
                    Set<Movie> movies = movieConverter.convertDtosToModels(moviesDto.getMovies());
                    movies.forEach(System.out::println);
                });
                Optional.of(command).filter(e->e==6).ifPresent(go->{this.addClientDialog();});
                Optional.of(command).filter(e->e==7).ifPresent(go->{
                    this.removeClientDialog();
                });
                Optional.of(command).filter(e->e==8).ifPresent(go->{this.updateClientDialog();});
                Optional.of(command).filter(e->e==9).ifPresent(go->{this.filterClientsDialog();});
                Optional.of(command).filter(e->e==10).ifPresent(go->{
                    ClientsDto clientsDto = restTemplate.getForObject(CLIENT_URL, ClientsDto.class);
                    Set<Client> clients = clientConverter.convertDtosToModels(clientsDto.getClients());
                    clients.forEach(System.out::println);
                });
                Optional.of(command).filter(e->e==11).ifPresent(go->{
                    this.addRental();
                });
                Optional.of(command).filter(e->e==12).ifPresent(go->{
                    this.removeRental();
                });
                Optional.of(command).filter(e->e==13).ifPresent(go->{
                    this.filterRentalsByClient();
                });
                Optional.of(command).filter(e->e==14).ifPresent(go->{
                    this.filterRentalsByMovie();
                });
                Optional.of(command).filter(e->e==15).ifPresent(go->{
                    try {
                        this.getMostRentedMovie();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                Optional.of(command).filter(e->e==16).ifPresent(go->{
                    RentalsDto rentalsDto = restTemplate.getForObject(RENTAL_URL, RentalsDto.class);
                    Set<Rental> rentals = rentalConverter.convertDtosToModels(rentalsDto.getRentals());
                    rentals.forEach(System.out::println);
                });
                Optional.of(command).filter(e->e==0).ifPresent(go->{
                    exit(0);
                });
            }
            catch(InputMismatchException | NumberFormatException e){
                System.out.println("Invalid input!\n");
            } catch(Exception e){
                System.out.print(e.getMessage());
            }
        }
    }
}
