package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Grade;
import ro.ubb.catalog.web.dto.GradeDto;


@Component
public class GradeConverter extends BaseConverter<Grade, GradeDto> {
    @Override
    public Grade convertDtoToModel(GradeDto dto) {
        Grade grade = Grade.builder()
                .studentID(dto.getStudentID())
                .problemID(dto.getProblemID())
                .grade(dto.getGrade())
                .build();
        grade.setId(dto.getId());
        return grade;
    }

    @Override
    public GradeDto convertModelToDto(Grade grade) {
        GradeDto dto = GradeDto.builder()
                .studentID(grade.getStudentID())
                .problemID(grade.getProblemID())
                .grade(grade.getGrade())
                .build();
        dto.setId(grade.getId());
        return dto;
    }
}

