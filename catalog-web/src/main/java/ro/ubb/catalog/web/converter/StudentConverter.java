package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Student;
import ro.ubb.catalog.web.dto.StudentDto;

@Component
public class StudentConverter extends BaseConverter<Student, StudentDto> {
    @Override
    public Student convertDtoToModel(StudentDto dto) {
        Student student = Student.builder()
                .name(dto.getName())
                .serialNumber(dto.getSerialNumber())
                .groupNumber(dto.getGroupNumber())
                .build();
        student.setId(dto.getId());
        return student;
    }

    @Override
    public StudentDto convertModelToDto(Student student) {
        StudentDto dto = StudentDto.builder()
                .name(student.getName())
                .serialNumber(student.getSerialNumber())
                .groupNumber(student.getGroupNumber())
                .build();
        dto.setId(student.getId());
        return dto;
    }
}
