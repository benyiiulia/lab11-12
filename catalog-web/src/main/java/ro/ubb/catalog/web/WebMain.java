package ro.ubb.catalog.web;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.core.service.StudentService;

public class WebMain {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("ro.ubb.catalog.web.config");
        StudentService studentService = (StudentService) context.getBean("StudentService");
        studentService.findAll().forEach(c -> System.out.println(c.toString()));
    }
}
