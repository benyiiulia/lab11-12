package ro.ubb.catalog.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.service.GradeService;
import ro.ubb.catalog.web.converter.GradeConverter;
import ro.ubb.catalog.web.converter.MovieConverter;
import ro.ubb.catalog.web.dto.*;

@RestController
public class GradeController {
    public static final Logger log= LoggerFactory.getLogger(GradeController.class);

    @Autowired
    private GradeService gradeService;

    @Autowired
    private GradeConverter gradeConverter;

    @Autowired
    private MovieConverter movieConverter;


    @RequestMapping(value = "/grades", method = RequestMethod.GET)
    GradesDto getGrades() {
        log.trace("getGrades");
        var res = gradeConverter
                .convertModelsToDtos(gradeService.findAll());
        return new GradesDto(res);
    }

    @RequestMapping(value = "/grades", method = RequestMethod.POST)
    GradeDto saveGrade(@RequestBody GradeDto gradeDto) {
        log.trace("saveGrade");
        return gradeConverter.convertModelToDto(gradeService.save(
                gradeConverter.convertDtoToModel(gradeDto)
        ));
    }

//    @RequestMapping(value = "/rentals/filterByClient/{id}", method = RequestMethod.GET)
//    RentalsDto filterByClient(@PathVariable Integer id) {
//        log.trace("filterByClient");
//        var res = rentalConverter.convertModelsToDtos(gradeService.filterRentalsByClient(id));
//        return new RentalsDto(res);
//    }
//
//    @RequestMapping(value = "/rentals/filterByMovie/{id}", method = RequestMethod.GET)
//    RentalsDto filterByMovie(@PathVariable Integer id) {
//        log.trace("filterByMovie");
//        var res = rentalConverter.convertModelsToDtos(gradeService.filterRentalsByMovie(id));
//        return new RentalsDto(res);
//    }

//    @RequestMapping(value = "/rentals/getMostRentedMovie", method = RequestMethod.GET)
//    MovieDto filterByMovie() throws Exception {
//        log.trace("getMostRentedMovie");
//        var res = movieConverter.convertModelToDto(gradeService.getMostRentedMovie());
//        return res;
//    }

    @RequestMapping(value = "/grades/{id}", method = RequestMethod.PUT)
    GradeDto updateGrade(@PathVariable Integer id,
                           @RequestBody GradeDto gradeDto) {
        log.trace("updateGrade");
        return gradeConverter.convertModelToDto( gradeService.update(
                gradeConverter.convertDtoToModel(gradeDto)));
    }

    @RequestMapping(value = "/grades/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteGrade(@PathVariable Integer id){
        log.trace("deleteGrade");
        gradeService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

