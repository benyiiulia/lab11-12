package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.ProblemService;
import ro.ubb.catalog.web.converter.ProblemConverter;
import ro.ubb.catalog.web.dto.*;

@RestController
public class ProblemController {
    public static final Logger log= LoggerFactory.getLogger(ProblemController.class);

    @Autowired
    private ProblemService problemService;

    @Autowired
    private ProblemConverter problemConverter;


    @RequestMapping(value = "/problems", method = RequestMethod.GET)
    ProblemsDto getProblems() {
        log.trace("getProblems");
        var res = problemConverter
                .convertModelsToDtos(problemService.findAll());
        return new ProblemsDto(res);
    }

//    @RequestMapping(value = "/movies/filterByName/{name}", method = RequestMethod.GET)
//    MoviesDto filterByMovie(@PathVariable String name) {
//        log.trace("filterByName");
//        var res = movieConverter.convertModelsToDtos(problemService.filterMoviesByName(name));
//        return new MoviesDto(res);
//    }

    @RequestMapping(value = "/problems", method = RequestMethod.POST)
    ProblemDto saveProblem(@RequestBody ProblemDto problemDto) {
        log.trace("saveProblem");
        return problemConverter.convertModelToDto(problemService.save(
                problemConverter.convertDtoToModel(problemDto)
        ));
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.PUT)
    ProblemDto updateProblem(@PathVariable Integer id,
                         @RequestBody ProblemDto problemDto) {
        log.trace("updateProblem");
        return problemConverter.convertModelToDto( problemService.update(
                problemConverter.convertDtoToModel(problemDto)));
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteProblem(@PathVariable Integer id){
        log.trace("deleteProblem");
        problemService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
