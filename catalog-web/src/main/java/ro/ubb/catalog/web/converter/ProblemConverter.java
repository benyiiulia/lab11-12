package ro.ubb.catalog.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Problem;
import ro.ubb.catalog.web.dto.ProblemDto;

@Component
public class ProblemConverter extends BaseConverter<Problem, ProblemDto> {
    @Override
    public Problem convertDtoToModel(ProblemDto dto) {
        Problem problem = Problem.builder()
                .text(dto.getText())
                .build();
        problem.setId(dto.getId());
        return problem;
    }

    @Override
    public ProblemDto convertModelToDto(Problem problem) {
        ProblemDto dto = ProblemDto.builder()
                .text(problem.getText())
                .build();
        dto.setId(problem.getId());
        return dto;
    }
}
