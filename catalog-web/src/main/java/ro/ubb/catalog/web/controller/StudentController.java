package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.StudentService;
import ro.ubb.catalog.web.converter.StudentConverter;
import ro.ubb.catalog.web.dto.*;


import java.util.List;

@RestController
public class StudentController {
    public static final Logger log= LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentConverter studentConverter;


    @RequestMapping(value = "/students", method = RequestMethod.GET)
    StudentsDto getStudents() {
        log.trace("getStudents");
        var res = studentConverter
                .convertModelsToDtos(studentService.findAll());
        return new StudentsDto(res);
    }

//    @RequestMapping(value = "/clients/filterByName/{name}", method = RequestMethod.GET)
//    ClientsDto filterByClient(@PathVariable String name) {
//        log.trace("filterByName");
//        var res = clientConverter.convertModelsToDtos(clientService.filterClientsByName(name));
//        return new ClientsDto(res);
//    }

    @RequestMapping(value = "/students", method = RequestMethod.POST)
    StudentDto saveStudent(@RequestBody StudentDto studentDto) {
        log.trace("saveStudent");
        return studentConverter.convertModelToDto(studentService.save(
                studentConverter.convertDtoToModel(studentDto)
        ));
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.PUT)
    StudentDto updateStudent(@PathVariable Integer id,
                           @RequestBody StudentDto studentDto) {
        log.trace("updateClient");
        return studentConverter.convertModelToDto( studentService.update(
                studentConverter.convertDtoToModel(studentDto)));
    }

    @RequestMapping(value = "/students/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteStudent(@PathVariable Integer id){
        log.trace("deleteStudent");
        studentService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
